# UE4 Remote Steering Wheel #

This project is composed by two components: a UE4 plugin with a test project and the Android Studio project. The Android project sets up a UDP socket that broadcasts the smartphone orientation. The plugin reads on the UDP port 5555 any broadcast datagram and parse it to make the orientation available to the game. The UE4 test project contains a vehicle gameplay where the player is a kart and the steering wheel is driven by the smartphone.

This is a video test showing how it works: https://www.youtube.com/watch?v=lGlUustNGj0

![wheel_1.png](https://bitbucket.org/repo/RKg44a/images/1664180744-wheel_1.png)